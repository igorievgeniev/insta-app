export class CryptoUtils {
  public static UUID(): string {
    return this.format(this.generate(8), TYPE.UUID);
  }

  public static Token(): string {
    return this.format(this.generate(4), TYPE.TOKEN);
  }

  private static generate(arraySize: number): string[] {
    return Uint16Array && window.crypto && window.crypto.getRandomValues ?
      // If we have a cryptographically secure PRNG, use that
      [].map.call(window.crypto.getRandomValues(new Uint16Array(arraySize)), i => this.pad4(i)) :
      // Otherwise, just use Math.random
      [].map.call(Array(arraySize).fill(0), () => this.random4());
  }

  private static pad4(num: number): string {
    let ret = num.toString(16);
    while (ret.length < 4) {
      ret = '0' + ret;
    }
    return ret;
  }

  private static random4(): string {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  private static format(data: string[], type: TYPE = TYPE.UUID): string {
    switch (type) {
      case TYPE.UUID:
        // 36 symbols
        // example: 550e8400-e29b-41d4-a716-446655440000
        return [[data[0], data[1]].join(''), data[2], data[3], data[4], [data[5], data[6], data[7]].join('')].join('-');
      case TYPE.TOKEN:
        // 16 symbols
        // example: 449b17131187bb53
        return data.join('');
    }
  }
}

enum TYPE {
  UUID,
  TOKEN
}
