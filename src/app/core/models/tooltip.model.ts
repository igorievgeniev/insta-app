export interface Tooltip {
  x: number;
  y: number;
  text: string;
}
