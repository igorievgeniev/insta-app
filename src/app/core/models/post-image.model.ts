import { Tooltip } from './tooltip.model';

export interface PostImage {
  imageUrl: string;
  tooltips: Tooltip[];
}
