import { PostImage } from '@core/models/post-image.model';

export interface Post extends PostImage {
  created: number;
  id: number;
  owner: string;
}
