import { User } from '@core/models/user.model';

export interface AuthUser extends User {
  email?: string;
  password: string;
}
