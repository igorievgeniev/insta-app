import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { AuthUser } from '@core/models/auth-user.model';
import { CryptoUtils } from '@core/helpers/crypto.utils';
import { PostsStub } from '@core/stubs/posts.stub';
import { Post } from '@core/models/post.model';
import { PostImage } from '@core/models/post-image.model';

@Injectable()
export class BackendInterceptor implements HttpInterceptor {

  constructor() {
    // init posts data
    if (!localStorage.getItem('posts')) {
      localStorage.setItem('posts', JSON.stringify(PostsStub));
    }

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // simulate server api call
    return of(null)
      .pipe(mergeMap(() => {

          // log in
          if (request.url.endsWith('/users/login') && request.method === 'POST') {
            const user = this.getUser({username: request.body.username});
            if (user && user.password === request.body.password) {
              const {username, token} = user;
              return of(new HttpResponse({status: 200, body: {username, token}}));
            } else {
              return throwError({message: 'Username or password is incorrect'});
            }
          }

          // sign up
          if (request.url.endsWith('/users') && request.method === 'POST') {
            if (!this.getUser({username: request.body.username})) {
              const user = this.createUser(request.body);
              const {username, token} = user;
              return of(new HttpResponse({status: 200, body: {username, token}}));
            } else {
              return throwError({message: 'Username is already in use'});
            }
          }

          // authorized user
          if (request.url.endsWith('/users') && request.method === 'GET') {
            const token = request.headers.get('Authorization');
            const user = this.getUser({token});

            if (token && user) {
              const {username} = user;
              return of(new HttpResponse({status: 200, body: {username, token}}));
            } else {
              return throwError({message: 'Unauthorised'});
            }
          }

          // get posts
          if (request.url.endsWith('/posts') && request.method === 'GET') {
            const token = request.headers.get('Authorization');
            const user = this.getUser({token});

            if (token && user) {
              const size = Number(request.params.get('size'));
              const index = Number(request.params.get('page'));
              const posts = this.getAllPosts().sort((a, b) => b.created - a.created);
              return of(new HttpResponse({status: 200, body: this.getChunk(posts, size, index)}));
            } else {
              return throwError({message: 'Unauthorised'});
            }
          }

          // get post
          if (/posts\/\d+$/.test(request.url) && request.method === 'GET') {
            const id = Number(request.url.match(/posts\/(\d+$)/)[1]);
            const token = request.headers.get('Authorization');
            const user = this.getUser({token});
            const post = this.getPost(id);

            if (token && user) {
              if (!post) {
                return throwError({message: 'Post not found'});
              } else {
                return of(new HttpResponse({status: 200, body: post}));
              }
            } else {
              return throwError({message: 'Unauthorised'});
            }
          }

          // create post
          if (request.url.endsWith('/posts') && request.method === 'POST') {
            const token = request.headers.get('Authorization');
            const user = this.getUser({token});

            if (token && user) {
              return of(new HttpResponse({status: 200, body: this.createPost(request.body, user)}));
            } else {
              return throwError({message: 'Unauthorised'});
            }
          }

          // update post
          if (/posts\/\d+$/.test(request.url) && request.method === 'PUT') {
            const id = Number(request.url.match(/posts\/(\d+$)/)[1]);
            const token = request.headers.get('Authorization');
            const user = this.getUser({token});
            const post = this.getPost(id);

            if (token && user) {
              if (!post) {
                return throwError({message: 'Post not found'});
              } else if (post.owner === user.username) {
                return of(new HttpResponse({status: 200, body: this.updatePost(id, request.body)}));
              } else {
                return throwError({message: 'You don\'t have permission to edit that post'});
              }
            } else {
              return throwError({message: 'Unauthorised'});
            }
          }

          // delete post
          if (/posts\/\d+$/.test(request.url) && request.method === 'DELETE') {
            const id = Number(request.url.match(/posts\/(\d+$)/)[1]);
            const token = request.headers.get('Authorization');
            const user = this.getUser({token});
            const post = this.getPost(id);

            if (token && user) {
              if (!post) {
                return throwError({message: 'Post not found'});
              } else if (post.owner === user.username) {
                return of(new HttpResponse({status: 200, body: this.deletePost(id)}));
              } else {
                return throwError({message: 'You don\'t have permission to edit that post'});
              }
            } else {
              return throwError({message: 'Unauthorised'});
            }
          }

          return next.handle(request);
        })
      )
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());
  }

  private getAllPosts(): Post[] {
    return JSON.parse(localStorage.getItem('posts')) || [];
  }

  private getPost(id: number): Post {
    return JSON.parse(localStorage.getItem('posts')).find(post => post.id === id);
  }

  private createPost(data: PostImage, user: AuthUser): Post {
    const posts = JSON.parse(localStorage.getItem('posts'));
    const newPost = {
      ...data,
      owner: user.username,
      created: new Date().getTime(),
      id: posts.length
    };

    posts.push(newPost);
    localStorage.setItem('posts', JSON.stringify(posts));

    return newPost;
  }

  private updatePost(id: number, data: PostImage): Post {
    const posts = JSON.parse(localStorage.getItem('posts'));
    const post = this.getPost(id);
    const updatedPost = {...post, ...data};

    posts.splice(posts.findIndex(item => item.id === post.id), 1, updatedPost);
    localStorage.setItem('posts', JSON.stringify(posts));

    return updatedPost;
  }

  private deletePost(id: number): Post {
    const posts = JSON.parse(localStorage.getItem('posts'));
    const post = this.getPost(id);

    localStorage.setItem('posts', JSON.stringify(posts.filter(item => item.id !== id)));

    return post;
  }

  private getUser(options: { username?: string, token?: string }): AuthUser {
    const users: AuthUser[] = JSON.parse(localStorage.getItem('users')) || [];
    return users.find(item => options.token === item.token || item.username === options.username);
  }

  private createUser(data: AuthUser): AuthUser {
    const users: AuthUser[] = JSON.parse(localStorage.getItem('users')) || [];
    const newUser: AuthUser = {...data, token: CryptoUtils.Token()};

    users.push(newUser);
    localStorage.setItem('users', JSON.stringify(users));

    return newUser;
  }

  private getChunk<T>(array: T[], chunkSize: number = 0, index: number = 0): T[] {
    return array.slice(chunkSize * index, (chunkSize * index) + chunkSize);
  }
}
