import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtService } from '@core/services/jwt.service';
import { environment } from '@env/environment';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private jwtService: JwtService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = new HttpHeaders({
      'Authorization': this.jwtService.getToken(),
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });

    return next.handle(request.url.startsWith(environment.api) ? request.clone({headers}) : request);
  }
}
