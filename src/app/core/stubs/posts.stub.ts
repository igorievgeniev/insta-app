/*tslint:disable:max-line-length*/
export const PostsStub = [
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688606/pexels-photo-257840_kvn409.jpg',
    'tooltips': [
      {
        'x': 65.43,
        'y': 49,
        'text': 'apple'
      }
    ],
    'owner': 'jennifer',
    'created': 1531688626222,
    'id': 14
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688574/BMW-m4-convertible-images-and-videos-1920x1200-06.jpg.asset.1510607725528_xjklve.jpg',
    'tooltips': [
      {
        'x': 30,
        'y': 69.67,
        'text': 'bmw'
      }
    ],
    'owner': 'jennifer',
    'created': 1531688592380,
    'id': 13
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688472/beach_shutterstock-1531521955-6002_w5fe4t.jpg',
    'tooltips': [
      {
        'x': 12.17,
        'y': 78,
        'text': 'vacation'
      }
    ],
    'owner': 'jennifer',
    'created': 1531688498335,
    'id': 12
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688424/228175628-picture-beautiful_e0qedh.jpg',
    'tooltips': [
      {
        'x': 26.96,
        'y': 61.33,
        'text': 'sunset'
      }
    ],
    'owner': 'jennifer',
    'created': 1531688444678,
    'id': 11
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688357/cbh_rhul_arch1_0026v1_1_0_wz0hw2.jpg',
    'tooltips': [
      {
        'x': 37.39,
        'y': 21.33,
        'text': 'Home, sweet home'
      }
    ],
    'owner': 'williams',
    'created': 1531688404506,
    'id': 10
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688251/5G-and-IoT-A-Beautiful-Noise-696x428_ncwjqc.jpg',
    'tooltips': [
      {
        'x': 3.7,
        'y': 46,
        'text': 'start'
      },
      {
        'x': 83.7,
        'y': 46,
        'text': 'finish'
      }
    ],
    'owner': 'name',
    'created': 1531688288418,
    'id': 9
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688189/beautiful-and-Lovely-pictures-of-rain-34_gtodmq.jpg',
    'tooltips': [
      {
        'x': 44.78,
        'y': 87.33,
        'text': 'is it pear?'
      }
    ],
    'owner': 'name',
    'created': 1531688240263,
    'id': 8
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688123/beautiful-planet-1_ly6zic.jpg',
    'tooltips': [
      {
        'x': 42.83,
        'y': 39.33,
        'text': 'Night'
      }
    ],
    'owner': 'jennifer',
    'created': 1531688150779,
    'id': 7
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688092/1472861812254_lyeqjs.jpg',
    'tooltips': [
      {
        'x': 47.17,
        'y': 61,
        'text': 'here'
      }
    ],
    'owner': 'williams',
    'created': 1531688117391,
    'id': 6
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531688069/140630124420-11-canada-most-beautiful-places-horizontal-large-gallery_qls2bz.jpg',
    'tooltips': [
      {
        'x': 45.22,
        'y': 55,
        'text': 'Hey'
      }
    ],
    'owner': 'williams',
    'created': 1531688085045,
    'id': 5
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531687890/636668445498177870-Ashford-Castle_oym5rs.jpg',
    'tooltips': [
      {
        'x': 3.04,
        'y': 14.67,
        'text': 'left'
      },
      {
        'x': 82.39,
        'y': 14,
        'text': 'right'
      }
    ],
    'owner': 'someuser',
    'created': 1531687935352,
    'id': 4
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531687824/22-Beautiful-Photos-of-Mauritius-2_brwgxa.jpg',
    'tooltips': [
      {
        'x': 35.43,
        'y': 84,
        'text': 'what a place'
      }
    ],
    'owner': 'someuser',
    'created': 1531687848100,
    'id': 3
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531232908/samples/ecommerce/analog-classic.jpg',
    'tooltips': [
      {
        'text': 'Wow',
        'x': 50,
        'y': 80
      }
    ],
    'owner': 'smith',
    'created': 1520632800000,
    'id': 3
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531232919/samples/landscapes/architecture-signs.jpg',
    'tooltips': [
      {
        'text': 'Select direction',
        'x': 45,
        'y': 50
      }
    ],
    'owner': 'smith',
    'created': 1517781600000,
    'id': 2
  },
  {
    'imageUrl': 'https://res.cloudinary.com/dq6ioh1lq/image/upload/v1531232922/samples/ecommerce/leather-bag-gray.jpg',
    'tooltips': [],
    'owner': 'smith',
    'created': 1526245600000,
    'id': 1
  }
];
