import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { PostImage } from '@core/models/post-image.model';
import { Post } from '@core/models/post.model';

@Injectable()
export class PostService {
  constructor(private http: HttpClient) {
  }

  createPost(data: PostImage): Observable<Post> {
    return this.http.post<Post> (`${environment.api}/posts`, data);
  }

  updatePost(id: number, data: PostImage): Observable<Post> {
    return this.http.put<Post> (`${environment.api}/posts/${id}`, data);
  }

  deletePost(id: number): Observable<Post> {
    return this.http.delete<Post>(`${environment.api}/posts/${id}`);
  }

  getPost(id: number): Observable<Post> {
    return this.http.get<Post>(`${environment.api}/posts/${id}`);
  }

  getPosts(page: number, size: number = 10): Observable<Post[]> {
    const params = new HttpParams()
      .set('size', String(size))
      .set('page', String(page));

    return this.http.get<Post[]>(`${environment.api}/posts`, {params});
  }
}
