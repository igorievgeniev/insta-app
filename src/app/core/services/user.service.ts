import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';
import { JwtService } from '@core/services/jwt.service';
import { User } from '@core/models/user.model';
import { AuthUser } from '@core/models/auth-user.model';
import { environment } from '@env/environment';

@Injectable()
export class UserService {
  public currentUser$: Observable<User>;
  public isAuthenticated$: Observable<boolean>;

  private currentUserSubject = new BehaviorSubject<User>(null as User);
  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);

  constructor(private http: HttpClient,
              private jwtService: JwtService) {
    this.currentUser$ = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
    this.isAuthenticated$  = this.isAuthenticatedSubject.asObservable();
  }

  populate(): void {
    if (this.jwtService.getToken()) {
      this.http.get<User>(`${environment.api}/users`).subscribe(user => this.setAuth(user), err => this.purgeAuth());
    } else {
      this.purgeAuth();
    }
  }

  signUp(credentials: AuthUser): Observable<User> {
    return this.http.post<User> (`${environment.api}/users`, credentials).pipe(tap(user => this.setAuth(user)));
  }

  logIn(credentials: AuthUser): Observable<User> {
    return this.http.post<User>(`${environment.api}/users/login`, credentials).pipe(tap(user => this.setAuth(user)));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.getValue();
  }

  purgeAuth(): void {
    this.jwtService.destroyToken();
    this.currentUserSubject.next(null as User);
    this.isAuthenticatedSubject.next(false);
  }

  private setAuth(user: User): void {
    this.jwtService.saveToken(user.token);
    this.currentUserSubject.next(user);
    this.isAuthenticatedSubject.next(true);
  }
}
