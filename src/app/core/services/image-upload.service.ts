import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { ImageUploadResponse } from '../models/image-upload-response.model';
import { Observable } from 'rxjs/index';
import { environment } from '@env/environment';

@Injectable()
export class ImageUploadService {
  private static IMAGE_CLOUD_NAME = 'dq6ioh1lq';
  private static IMAGE_UPLOAD_PRESET = 'imageUpload';

  constructor(private http: HttpClient) {
  }

  uploadImage(file: File): Observable<HttpEvent<ImageUploadResponse>> {
    const url = `${environment.imageUploadApi}/${ImageUploadService.IMAGE_CLOUD_NAME}/upload`;
    const formData = new FormData();

    formData.append('file', file);
    formData.append('upload_preset', ImageUploadService.IMAGE_UPLOAD_PRESET);

    return this.http.post<ImageUploadResponse>(url, formData, {reportProgress: true, observe: 'events'});
  }
}
