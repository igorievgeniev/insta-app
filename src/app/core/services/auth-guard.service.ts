import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { UserService } from '@core/services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private userService: UserService,
              private router: Router) {
  }

  canActivate(): Observable<boolean> {
    return this.userService.isAuthenticated$.pipe(take(1), tap(isAuth => {
      if (!isAuth) {
        this.router.navigate(['auth']);
      }
    }));
  }
}
