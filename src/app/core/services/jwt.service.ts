import { Injectable } from '@angular/core';

@Injectable()
export class JwtService {
  private key = 'jwtToken';

  getToken(): string {
    return sessionStorage.getItem(this.key);
  }

  saveToken(token: string): void {
    sessionStorage.setItem(this.key, token);
  }

  destroyToken(): void {
    sessionStorage.removeItem(this.key);
  }

}
