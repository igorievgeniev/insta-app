import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthorizationInterceptor } from '@core/interceptors/authorization.interceptor';
import { BackendInterceptor } from '@core/interceptors/backend.interceptor';
import { UserService } from '@core/services/user.service';
import { JwtService } from '@core/services/jwt.service';
import { PostService } from '@core/services/post.service';
import { ImageUploadService } from '@core/services/image-upload.service';
import { PostResolver } from '@core/services/post-resolver.service';
import { AuthGuard } from '@core/services/auth-guard.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true},
    // backend simulation
    {provide: HTTP_INTERCEPTORS, useClass: BackendInterceptor, multi: true},
    JwtService,
    UserService,
    PostService,
    ImageUploadService,
    PostResolver,
    AuthGuard
  ]
})
export class CoreModule {
}
