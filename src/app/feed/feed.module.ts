import { NgModule } from '@angular/core';
import { FeedComponent } from './components/feed.component';
import { FeedRoutingModule } from './feed-routing.module';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    FeedRoutingModule
  ],
  declarations: [FeedComponent]
})
export class FeedModule {
}
