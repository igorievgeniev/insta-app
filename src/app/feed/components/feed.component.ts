import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject, fromEvent } from 'rxjs';
import { auditTime, filter, map, takeWhile } from 'rxjs/operators';
import { UserService } from '@core/services/user.service';
import { PostService } from '@core/services/post.service';
import { User } from '@core/models/user.model';
import { Post } from '@core/models/post.model';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, AfterViewInit, OnDestroy {
  public user$: Observable<User>;
  public posts$ = new BehaviorSubject<Post[]>([]);

  private posts: Post[] = [];
  private page = 0;
  private alive = true;

  constructor(private element: ElementRef,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private userService: UserService,
              private postService: PostService) {
  }

  ngOnInit(): void {
    this.user$ = this.userService.currentUser$;
    this.loadPosts();
  }

  ngAfterViewInit(): void {
    const throttleTime = 50;

    fromEvent<Event>(this.element.nativeElement, 'scroll').pipe(
      takeWhile(() => this.alive),
      auditTime(throttleTime),
      map((event: Event) => event.target),
      filter((element: HTMLElement) => (element.scrollHeight - element.clientHeight - element.scrollTop) === 0)
    )
    .subscribe(() => {
      this.page++;
      this.loadPosts();
    });
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  onEdit(id: number): void {
    this.router.navigate(['posts', id]);
  }

  onDelete(id: number): void {
    this.postService.deletePost(id).subscribe(() => {
      this.posts = this.posts.filter(post => post.id !== id);
      this.posts$.next(this.posts);
    });
  }

  private loadPosts(): void {
    this.postService.getPosts(this.page).pipe(filter(data => !!data.length)).subscribe(data => {
      this.posts = this.posts.concat(data);
      this.posts$.next(this.posts);
    });
  }
}
