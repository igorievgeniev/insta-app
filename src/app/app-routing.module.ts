import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@core/services/auth-guard.service';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: 'app/auth/auth.module#AuthModule'
  },
  {
    path: 'posts',
    canActivate: [AuthGuard],
    loadChildren: 'app/wizard/wizard.module#WizardModule'
  },
  {
    path: 'feed',
    canActivate: [AuthGuard],
    loadChildren: 'app/feed/feed.module#FeedModule'
  },
  {
    path: '**',
    redirectTo: 'feed',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
