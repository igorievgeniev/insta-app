import { animate, transition, style } from '@angular/animations';

export const fadeInOut = [
  transition(':enter', [
    style({opacity: 0}),
    animate('400ms ease-in-out', style({opacity: 1}))
  ]),

  transition(':leave', [
    style({opacity: 1}),
    animate('300ms ease-in-out', style({opacity: 0}))
  ])
];
