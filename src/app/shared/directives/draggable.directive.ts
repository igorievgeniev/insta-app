import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

interface Position {
  x: number;
  y: number;
}

@Directive({
  selector: '[appDraggable]'
})
export class DraggableDirective {
  @Output()
  public dragStart = new EventEmitter<PointerEvent>();

  @Output()
  public dragMove = new EventEmitter<PointerEvent>();

  @Output()
  public dragEnd = new EventEmitter<PointerEvent>();

  @HostBinding('style.transform')
  get transform(): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(`translate(${this.position.x}px, ${this.position.y}px)`);
  }

  public position: Position = {x: 0, y: 0};

  private startPosition: Position;
  private dragging = false;

  constructor(public element: ElementRef,
              private sanitizer: DomSanitizer) {
  }

  @HostListener('pointerdown', ['$event'])
  onDragStart(event: PointerEvent): void {
    this.dragging = true;
    this.startPosition = {
      x: event.clientX - this.position.x,
      y: event.clientY - this.position.y
    };
    this.dragStart.emit(event);
  }

  @HostListener('document:pointermove', ['$event'])
  onDragMove(event: PointerEvent): void {
    if (!this.dragging) {
      return;
    }

    this.position.x = event.clientX - this.startPosition.x;
    this.position.y = event.clientY - this.startPosition.y;
    this.dragMove.emit(event);
  }

  @HostListener('document:pointerup', ['$event'])
  onDragEnd(event: PointerEvent): void {
    if (!this.dragging) {
      return;
    }

    this.dragging = false;
    this.dragEnd.emit(event);
  }
}
