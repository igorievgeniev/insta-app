import {
  AfterViewInit,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  QueryList
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DraggableDirective } from './draggable.directive';
import { Tooltip } from '@core/models/tooltip.model';

interface Boundaries {
  minX: number;
  maxX: number;
  minY: number;
  maxY: number;
}

@Directive({
  selector: '[appTooltipArea]'
})
export class TooltipAreaDirective implements AfterViewInit {
  @Output()
  public change = new EventEmitter<Tooltip[]>();

  @Input()
  set appTooltipArea(tooltips: Tooltip[]) {
    this.tooltips = tooltips;
  }

  @ContentChildren(DraggableDirective)
  public draggableItems: QueryList<DraggableDirective>;

  private viewClientRect: ClientRect;
  private boundaries: Boundaries;
  private subscriptions: Subscription[] = [];

  private tooltips: Tooltip[];

  constructor(private element: ElementRef) {
  }

  ngAfterViewInit(): void {
    this.draggableItems.changes.subscribe(() => {
      this.subscriptions.forEach(s => s.unsubscribe());

      this.draggableItems.forEach(draggable => {
        this.subscriptions.push(draggable.dragStart.subscribe(() => this.measureBoundaries(draggable)));
        this.subscriptions.push(draggable.dragMove.subscribe(() => this.maintainBoundaries(draggable)));
        this.subscriptions.push(draggable.dragEnd.subscribe(() => this.updatePosition(draggable)));
      });
    });

    this.draggableItems.notifyOnChanges();
  }

  private measureBoundaries(draggable: DraggableDirective): void {
    const draggableClientRect = draggable.element.nativeElement.getBoundingClientRect();
    this.viewClientRect = this.element.nativeElement.getBoundingClientRect();

    this.boundaries = {
      minX: this.viewClientRect.left - draggableClientRect.left + draggable.position.x,
      maxX: this.viewClientRect.right - draggableClientRect.right + draggable.position.x,
      minY: this.viewClientRect.top - draggableClientRect.top + draggable.position.y,
      maxY: this.viewClientRect.bottom - draggableClientRect.bottom + draggable.position.y
    };
  }

  private maintainBoundaries(draggable: DraggableDirective): void {
    draggable.position.x = Math.max(this.boundaries.minX, draggable.position.x);
    draggable.position.x = Math.min(this.boundaries.maxX, draggable.position.x);
    draggable.position.y = Math.max(this.boundaries.minY, draggable.position.y);
    draggable.position.y = Math.min(this.boundaries.maxY, draggable.position.y);
  }

  private updatePosition(draggable: DraggableDirective): void {
    const draggableIndex = this.draggableItems.toArray().findIndex(item => item === draggable);
    const tooltip = this.tooltips[draggableIndex];

    if (tooltip) {
      tooltip.x = this.decimal(tooltip.x + this.normalizePosition(this.viewClientRect.width, draggable.position.x));
      draggable.position.x = 0;
      tooltip.y = this.decimal(tooltip.y + this.normalizePosition(this.viewClientRect.height, draggable.position.y));
      draggable.position.y = 0;
    }

    this.change.emit([...this.tooltips]);
  }

  private normalizePosition(max: number, position: number): number {
    return position * 100 / max;
  }

  private decimal(value: number): number {
    return Math.round(value * 100) / 100;
  }
}
