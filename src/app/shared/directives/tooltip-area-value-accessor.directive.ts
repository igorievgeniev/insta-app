import { ChangeDetectorRef, Directive, forwardRef, OnDestroy } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Tooltip } from '@core/models/tooltip.model';
import { ImageAreaComponent } from '@shared/components/image-area/image-area.component';

export const TOOLTIP_AREA_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => TooltipAreaValueAccessorDirective),
  multi: true
};

@Directive({
  selector: 'app-image-area[formControlName][appTooltipAreaValueAccessor]',
  providers: [TOOLTIP_AREA_VALUE_ACCESSOR]
})
export class TooltipAreaValueAccessorDirective implements ControlValueAccessor, OnDestroy {
  private destroy$ = new Subject();
  private innerValue: Tooltip[] = null;

  constructor(private host: ImageAreaComponent,
              private cdRef: ChangeDetectorRef) {
    if (this.host.change) {
      this.host.change.pipe(takeUntil(this.destroy$)).subscribe(value => {
        this.writeValue(value);
        this._onChange(value);
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  writeValue(value: Tooltip[]): void {
    if (value !== this.innerValue) {
      this.innerValue = value;
      this.host.tooltips = this.innerValue;
      this.cdRef.markForCheck();
    }
  }

  registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }

  private _onChange = (_: any) => {};
  private _onTouched = () => {};
}
