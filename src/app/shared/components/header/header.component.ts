import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '@core/services/user.service';
import { User } from '@core/models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  public user$: Observable<User>;

  constructor(private userService: UserService,
              private router: Router) {
    this.user$ = this.userService.currentUser$;
  }

  onLogout(): void {
    this.userService.purgeAuth();
    this.router.navigateByUrl('/auth');
  }
}
