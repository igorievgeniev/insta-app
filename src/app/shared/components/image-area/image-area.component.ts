import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { Tooltip } from '@core/models/tooltip.model';

@Component({
  selector: 'app-image-area',
  templateUrl: './image-area.component.html',
  styleUrls: ['./image-area.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageAreaComponent {
  @Output()
  public change = new EventEmitter<Tooltip[]>();

  @Input()
  public tooltips: Tooltip[] = [];

  @HostBinding('class.editable')
  @Input()
  public editable: boolean;

  @HostBinding('class.show-tooltips')
  public showTooltips: boolean;

  constructor() {
  }

  onChange(tooltips: Tooltip[]): void {
    this.change.emit(tooltips);
  }
}
