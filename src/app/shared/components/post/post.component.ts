import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { Post } from '@core/models/post.model';
import { fadeInOut } from '@shared/common/animations';
import { trigger } from '@angular/animations';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  animations: [trigger('animation', [...fadeInOut])],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent {
  @Output()
  public edit = new EventEmitter<number>();

  @Output()
  public delete = new EventEmitter<number>();

  @Input()
  public post: Post;

  @Input()
  public editable: boolean;

  @HostBinding('@animation')
  public animation: string;

  constructor() {
  }
}
