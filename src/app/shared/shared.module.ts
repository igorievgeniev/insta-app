import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import { HeaderComponent } from '@shared/components/header/header.component';
import { ImageAreaComponent } from '@shared/components/image-area/image-area.component';
import { TooltipAreaDirective } from '@shared/directives/tooltip-area.directive';
import { DraggableDirective } from '@shared/directives/draggable.directive';
import { TooltipAreaValueAccessorDirective } from './directives/tooltip-area-value-accessor.directive';
import { PostComponent } from './components/post/post.component';
import { TimeAgoPipe } from '@shared/pipes/time-ago.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
    MatCardModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    HeaderComponent,
    ImageAreaComponent,
    PostComponent,
    TooltipAreaValueAccessorDirective,
    TooltipAreaDirective,
    DraggableDirective,
    TimeAgoPipe
  ],
  exports: [
    // modules
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    // components
    HeaderComponent,
    ImageAreaComponent,
    PostComponent,

    // directives
    DraggableDirective,
    TooltipAreaDirective,
    TooltipAreaValueAccessorDirective,

    // pipes
    TimeAgoPipe
  ]
})
export class SharedModule {
}
