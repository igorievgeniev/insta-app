import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { take } from 'rxjs/operators';
import { UserService } from '@core/services/user.service';
import { AuthType } from '../models/auth-type.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthComponent implements OnInit {
  public readonly AuthType = AuthType;

  public authForm: FormGroup;
  public authType: AuthType;
  public isSubmitting = false;
  public hidePassword = true;
  public auth: string;

  public loginText = 'Log in';
  public signUpText = 'Sign Up';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private cdRef: ChangeDetectorRef,
              private snackBar: MatSnackBar,
              private userService: UserService) {
    this.authForm = this.fb.group({
      'username': ['', Validators.minLength(3)],
      'password': ['', Validators.minLength(6)]
    });
  }

  ngOnInit(): void {
    this.route.data.pipe(take(1)).subscribe(data => {
      this.authType = data.authType || AuthType.LOGIN;

      this.auth = this.loginText;

      if (this.authType === AuthType.SIGNUP) {
        this.auth = this.signUpText;
        this.authForm.addControl(
          'email',
          new FormControl('', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')])
        );
      }
    });
  }

  submitForm(): void {
    this.isSubmitting = true;

    if (this.authType === AuthType.SIGNUP) {
      this.userService
        .signUp(this.authForm.value)
        .subscribe(() => this.handleSuccess(), error => this.handleError(error.message));
    } else {
      this.userService
        .logIn(this.authForm.value)
        .subscribe(() => this.handleSuccess(), error => this.handleError(error.message));
    }
  }

  private handleSuccess(): void {
    this.router.navigateByUrl('/');
  }

  private handleError(message: string): void {
    this.snackBar.open(message, 'Ok');
    this.isSubmitting = false;
    this.cdRef.markForCheck();
  }
}
