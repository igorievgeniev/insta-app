import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth.component';
import { AuthType } from './models/auth-type.model';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    data: {authType: AuthType.LOGIN}
  },
  {
    path: 'signup',
    component: AuthComponent,
    data: {authType: AuthType.SIGNUP}
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
