import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControl } from '@angular/forms/src/model';
import { MatDialog } from '@angular/material';
import { filter } from 'rxjs/operators';
import { PostService } from '@core/services/post.service';
import { ImageUploadService } from '@core/services/image-upload.service';
import { Tooltip } from '@core/models/tooltip.model';
import { Post } from '@core/models/post.model';
import { UserService } from '@core/services/user.service';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WizardComponent implements OnInit {
  @ViewChild('fileInput')
  public fileInput: ElementRef;

  @ViewChild('addTooltipDialog')
  public addTooltipDialog: TemplateRef<any>;

  public imageAlt = 'Your post image';
  public uploading: boolean;
  public uploadingProgress: number;
  public dialogData: Tooltip;
  public editing: boolean;
  public post: Post;
  public postPreview: Post;

  public fileUploadFormGroup: FormGroup;
  public tooltipFormGroup: FormGroup;

  get imageUrlControl(): AbstractControl {
    return this.fileUploadFormGroup.get('imageUrl');
  }

  get tooltipsControl(): AbstractControl {
    return this.tooltipFormGroup.get('tooltips');
  }

  constructor(private imageUploadService: ImageUploadService,
              private postService: PostService,
              private userService: UserService,
              private dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private cdRef: ChangeDetectorRef) {
    if (this.route.snapshot.data && this.route.snapshot.data.post) {
      this.post = this.route.snapshot.data.post;
      this.editing = true;
    }
  }

  ngOnInit(): void {
    this.fileUploadFormGroup = this.fb.group({imageUrl: [null, Validators.required]});
    this.tooltipFormGroup = this.fb.group({tooltips: [null]});

    this.postPreview = {
      id: 0,
      created: new Date().getTime(),
      owner: this.userService.getCurrentUser().username,
      tooltips: [],
      imageUrl: null
    };

    if (this.post) {
      const {imageUrl, tooltips} = this.post;
      this.fileUploadFormGroup.patchValue({imageUrl});
      this.tooltipFormGroup.patchValue({tooltips});

      this.postPreview = {...this.postPreview, tooltips};
      this.postPreview = {...this.postPreview, imageUrl};
    }

    this.fileUploadFormGroup.valueChanges.subscribe(imageUrl => {
      this.postPreview = {...this.postPreview, ...imageUrl};
    });

    this.tooltipFormGroup.valueChanges.subscribe(tooltips => {
      this.postPreview = {...this.postPreview, ...tooltips};
    });
  }

  onFileChange(files: FileList): void {
    const reader = new FileReader();

    if (files.length) {
      const file = files[0];
      reader.readAsDataURL(file);
      reader.onload = () => this.imageUploading(file);
    }
  }

  onAddTooltip(): void {
    const dialogRef = this.dialog.open(this.addTooltipDialog);
    const tooltips = this.tooltipsControl.value || [];
    this.dialogData = {x: 0, y: 0, text: ''};
    dialogRef.afterClosed().pipe<Tooltip>(filter(tooltip => !!tooltip)).subscribe(tooltip => {
      this.tooltipsControl.patchValue([...tooltips, tooltip]);
      this.cdRef.markForCheck();
    });
  }

  onRemoveTooltip(tooltip: Tooltip): void {
    const tooltips = this.tooltipsControl.value;
    this.tooltipsControl.patchValue([...tooltips.filter(item => item !== tooltip)]);
    this.cdRef.markForCheck();
  }

  onSubmit(): void {
    const postImage = {...this.fileUploadFormGroup.value, ...this.tooltipFormGroup.value};

    if (this.editing) {
      const {id} = this.post;
      this.postService.updatePost(id, postImage).subscribe(() => this.router.navigate(['/feed']));
    } else {
      this.postService.createPost(postImage).subscribe(() => this.router.navigate(['/feed']));
    }
  }

  private imageUploading(file: File): void {
    this.imageUploadService.uploadImage(file).subscribe(event => {
      switch (event.type) {
        case HttpEventType.Sent:
          this.uploadingProgress = 0;
          this.uploading = true;
          break;

        case HttpEventType.UploadProgress:
          this.uploadingProgress = Math.round(event.loaded / event.total * 100);
          break;

        case HttpEventType.Response:
          this.uploading = false;
          this.imageUrlControl.patchValue(event.body.secure_url);
          break;
      }

      this.cdRef.markForCheck();
    });
  }
}
