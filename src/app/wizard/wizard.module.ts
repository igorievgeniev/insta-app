import { NgModule } from '@angular/core';
import {
MatButtonModule,
MatCardModule,
MatDialogModule,
MatFormFieldModule,
MatIconModule,
MatInputModule,
MatProgressBarModule,
MatStepperModule,
MatTooltipModule
} from '@angular/material';
import { WizardComponent } from './components/wizard.component';
import { SharedModule } from '@shared/shared.module';
import { WizardRoutingModule } from './wizard-routing.module';

@NgModule({
  imports: [
    SharedModule,
    MatCardModule,
    MatStepperModule,
    MatProgressBarModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTooltipModule,
    WizardRoutingModule
  ],
  declarations: [
    WizardComponent
  ]
})
export class WizardModule {
}
