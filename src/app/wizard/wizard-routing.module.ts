import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WizardComponent } from './components/wizard.component';
import { PostResolver } from '@core/services/post-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: WizardComponent,
  },
  {
    path: ':id',
    component: WizardComponent,
    resolve: {
      post: PostResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardRoutingModule {
}
