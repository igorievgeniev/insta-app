export const environment = {
  production: true,
  api: 'https://backend.com',
  imageUploadApi: 'https://api.cloudinary.com/v1_1'
};
